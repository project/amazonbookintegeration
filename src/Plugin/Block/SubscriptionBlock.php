<?php

namespace Drupal\amazon_integeration\Plugin\Block;

use Drupal\user\Entity\User;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a generic Search block.
 *
 * @Block(
 *   id = "subscription_block",
 *   admin_label = @Translation("Amazon Books of user"),
 * )
 */
class SubscriptionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $current_path = \Drupal::service('path.current')->getPath();
    $path = \Drupal::service('path.alias_manager')->getPathByAlias($current_path);
    if (preg_match('/user\/(\d+)/', $path, $matches)) {
      $uid = $matches[1];
      $account = User::load($uid);
      $profile = get_user_profile($account);
      $first_name = $profile->field_author_first_name->value;
      $last_name = $profile->field_author_last_name->value;
      $name = $first_name . ' ' . $last_name . ' Merchandise';
    }
    else {
      $uid = \Drupal::currentUser()->id();
      $account = User::load($uid);
      $profile = get_user_profile($account);
      $first_name = $profile->field_author_first_name->value;
      $last_name = $profile->field_author_last_name->value;
      $name = $first_name . ' ' . $last_name . ' Merchandise';
    }
    $query = \Drupal::database()->select('amazon_books', 'amazon');
    $query->fields('amazon', ['image', 'title', 'price', 'link', 'asin']);
    $query->condition('amazon.uid', $uid, '=');
    $result = $query->execute()->fetchAll();

    if ($uid == \Drupal::currentUser()->id()) {
      $isDelete = 1;
    }
    if (!empty($result)) {
      return [
        '#theme' => 'custom_user_books',
        '#title' => $name,
        '#books' => $result,
        '#isDelete' => $isDelete,
        '#cache' => ['max-age' => 0],
      ];
    }
    else {
      return;
    }
  }

}
