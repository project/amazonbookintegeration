<?php

namespace Drupal\amazon_integeration\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a generic Search block.
 *
 * @Block(
 *   id = "site_search_block",
 *   admin_label = @Translation("Amazon Book Search Block"),
 * )
 */
class SearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\amazon_integeration\Form\BookSearch');
    return $form;
  }

}
