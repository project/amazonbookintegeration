<?php

namespace Drupal\amazon_integeration;

/**
 * This class is used to get the response from Amazon API.
 */
class AmazonSearch {

  /**
   * This is the function that passes keyword to API.
   */
  public function getData($search_term, $search_index) {
    $response = AmazonSearch::getAmazonBook("com", $search_term, $search_index);
    return $response;
  }

  /**
   * Region code and Product ASIN.
   */
  public function getAmazonBook($region, $search_term, $search_index) {
    $keys = \Drupal::request();
    $pageSearch = $keys->get('page');
    if (empty($pageSearch)) {
      $pageSearch = 1;
    }
    $xml = AmazonSearch::awsSignedRequest($region, [
      "Operation" => "ItemSearch",
      "Keywords" => $search_term,
      "IncludeReviewsSummary" => FALSE,
      "ResponseGroup" => "ItemAttributes,Images",
      "SearchIndex" => $search_index,
      "ItemPage" => $pageSearch,
    ]);
    $item = $xml->Items->Item;
    $i = 0;
    $response = [];
    if (!empty($xml->Items->TotalPages)) {
      $response['TotalPages'] = $xml->Items->TotalPages->__toString();
    }
    foreach ($item as $result) {
      $response[$i]['image'] = htmlentities((string) $result->MediumImage->URL);
      $response[$i]['title'] = htmlentities((string) $result->ItemAttributes->Title);
      $response[$i]['author'] = htmlentities((string) $result->ItemAttributes->Author[0]);
      $response[$i]['binding'] = htmlentities((string) $result->ItemAttributes->Binding);
      $response[$i]['price'] = htmlentities((string) $result->ItemAttributes->ListPrice->FormattedPrice);
      $response[$i]['asin'] = htmlentities((string) $result->ASIN);
      $response[$i]['buyonline'] = htmlentities((string) $result->DetailPageURL);
      $i++;
    }
    return $response;
  }

  /**
   * Initiate CURL request.
   */
  public function getPage($url) {

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    $html = curl_exec($curl);
    curl_close($curl);
    return $html;
  }

  /**
   * Get the config data from amazon settings form.
   */
  public function awsSignedRequest($region, $params) {

    $config = \Drupal::config('amazon.settings');
    $public_key = $config->get('amazon_aws_access_key');
    $private_key = $config->get('amazon_aws_secret_access_key');

    $method = "GET";
    $host = "ecs.amazonaws." . $region;
    $host = "webservices.amazon." . $region;
    $uri = "/onca/xml";

    $params["Service"] = "AWSECommerceService";
    // Put your Affiliate Code here.
    $params["AssociateTag"] = $config->get('amazon_associate_id');
    $params["AWSAccessKeyId"] = $public_key;
    $params["Timestamp"] = gmdate("Y-m-d\TH:i:s\Z");
    $params["Version"] = "2011-08-01";

    ksort($params);

    $canonicalized_query = [];
    foreach ($params as $param => $value) {
      $param = str_replace("%7E", "~", rawurlencode($param));
      $value = str_replace("%7E", "~", rawurlencode($value));
      $canonicalized_query[] = $param . "=" . $value;
    }

    $canonicalized_query = implode("&", $canonicalized_query);

    $string_to_sign = $method . "\n" . $host . "\n" . $uri . "\n" . $canonicalized_query;
    $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $private_key, TRUE));
    $signature = str_replace("%7E", "~", rawurlencode($signature));

    $request = "http://" . $host . $uri . "?" . $canonicalized_query . "&Signature=" . $signature;
    $response = AmazonSearch::getPage($request);

    $pxml = @simplexml_load_string($response);
    if ($pxml === FALSE) {
      // No xml.
      return FALSE;
    }
    else {
      return $pxml;
    }
  }

}
