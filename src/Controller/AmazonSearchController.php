<?php

namespace Drupal\amazon_integeration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\amazon_integeration\AmazonSearch;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Route controller for search.
 */
class AmazonSearchController extends ControllerBase {

  /**
   * This method gets the result from API and passes to other form.
   */
  public function getResults() {
    $booksearchForm = \Drupal::formBuilder()->getForm('Drupal\amazon_integeration\Form\BookSearch');
    $keys = \Drupal::request();
    $results = AmazonSearch::getData($keys->get('Keywords'), $keys->get('SearchIndex'));
    $keyword = $keys->get('Keywords');
    $current_page = $keys->get('page');
    if (empty($current_page)) {
      $current_page = 1;
    }
    if ($results['TotalPages'] != 0) {
      $totalPages = $results['TotalPages'];
      unset($results['TotalPages']);
      $resultsform = \Drupal::formBuilder()->getForm('\Drupal\amazon_integeration\Form\AmazonResultsForm', $results);
    }

    return [
      '#theme' => 'custom_book_search',
      '#form' => $booksearchForm,
      '#results' => $resultsform,
      '#totalPages' => $totalPages,
      '#keyword' => $keyword,
      '#currentPage' => $current_page,
    ];
  }

  /**
   * Delete book from table once user clicks on delete button.
   */
  public function deleteBook($asin) {
    $uid = \Drupal::currentUser()->id();
    $query = \Drupal::database()->delete('amazon_books');
    $query->condition('asin', $asin);
    $query->condition('uid', $uid);
    $query->execute();

    $response = new RedirectResponse($_SERVER["HTTP_REFERER"]);
    $response->send();
    drupal_set_message(t('Item successfully Removed from your Store.'), 'status', TRUE);
  }

}
