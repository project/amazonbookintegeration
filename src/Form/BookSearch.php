<?php

namespace Drupal\amazon_integeration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Creating the search field to interact with Amazon.
 */
class BookSearch extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'book-search';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['keyword'] = [
      '#type' => 'textfield',
      '#size' => 60,
      '#id' => 'book-search-suggestions',
      '#default_value' => \Drupal::request()->get('Keywords'),
      '#attributes' => [
        'id' => 'site-search-suggestions',
        'class' => ['custom-search-input-box'],
      ],
    ];
    $form['add']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Apply'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $search = $form_state->getValue('keyword');

    $options = [
      'query' => ['Keywords' => $form_state->getValue('keyword'), 'SearchIndex' => 'Books'],
    ];
    $form_state->setRedirect('book_search.search', [], $options);

  }

}
