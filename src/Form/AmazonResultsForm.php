<?php

namespace Drupal\amazon_integeration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Manage Amazon Product Advertisment API settings.
 */
class AmazonResultsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazon_results';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'amazon_results.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $results = []) {

    $i = 0;
    foreach ($results as $result) {
      $form[$i]['checkbox-' . $i] = [
        '#type' => 'checkbox',
      ];
      $form[$i]['image'] = [
        '#markup' => '<img src = ' . $result['image'] . '/>',
      ];
      $form[$i]['title'] = [
        '#markup' => '<div>' . $result['title'] . '</div>',
      ];
      $form[$i]['author'] = [
        '#markup' => '<div> Author:' . $result['author'] . '</div>',
      ];
      $form[$i]['asin'] = [
        '#markup' => '<div> ASIN or ISBN-10:' . $result['asin'] . '</div>',
      ];
      $form[$i]['binding'] = [
        '#markup' => '<div> Binding:' . $result['binding'] . '</div>',
      ];
      $form[$i]['price'] = [
        '#markup' => '<div> List Price:' . $result['price'] . '</div>',
      ];
      $form[$i]['buyonline'] = [
        '#markup' => '<div>' . $result['buyonline'] . '</div>',
        '#type' => 'hidden',
      ];
      $form[$i]['image_hidden'] = [
        '#markup' => $result['image'],
        '#type' => 'hidden',
      ];
      $i++;
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Select'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $query = [];
    $uid = \Drupal::currentUser()->id();
    $books_added = [];
    foreach ($form_state->getValues() as $key => $value) {
      if ($value == 1) {
        $result = str_replace('checkbox-', '', $key);
        if (is_numeric($result)) {
          $image = $form[$result]['image_hidden']['#markup'];
          $title = strip_tags($form[$result]['title']['#markup']);
          $author = explode(':', strip_tags($form[$result]['author']['#markup']))[1];
          $binding = explode(':', strip_tags($form[$result]['binding']['#markup']))[1];
          $price = explode(':', strip_tags($form[$result]['price']['#markup']))[1];
          $asin = explode(':', strip_tags($form[$result]['asin']['#markup']))[1];
          $buyOnline = strip_tags($form[$result]['buyonline']['#markup']);
          $query = \Drupal::database()->merge('amazon_books')
            ->key([
              'uid' => $uid,
              'asin' => $asin,
            ])
            ->fields([
              'uid' => $uid,
              'image' => $image,
              'title' => $title,
              'author' => $author,
              'binding' => $binding,
              'price' => $price,
              'asin' => $asin,
              'link' => $buyOnline,
            ])
            ->execute();

          $books_added[] = $title;
        }
      }
    }
    unset(drupal_get_messages()['status']);
    foreach ($books_added as $books) {
      drupal_set_message($books . ' Successfully added to your profile.');
    }
    $form_state->setRedirect('book_search.search');
  }

}
