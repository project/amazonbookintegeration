<?php

namespace Drupal\amazon_integeration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Manage Amazon Product Advertisment API settings.
 */
class AmazonSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazon_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'amazon.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('amazon.settings');

    $form['message'] = [
      '#markup' => $this->t('Manage the Amazon Product Advertisment API Settings'),
    ];

    // Amazon Web Services Key || Textfield.
    $form['amazon_aws_access_key'] = [
      '#type' => 'textfield',
      '#title' => t('Amazon AWS Access Key ID'),
      '#description' => t('You must sign up for an Amazon AWS account to use the Product Advertising Service.'),
      '#default_value' => $config->get('amazon_aws_access_key'),
      '#required' => TRUE,
    ];

    // Amazon Web Servies Secret Key || Textfield.
    $form['amazon_aws_secret_access_key'] = [
      '#type' => 'textfield',
      '#title' => t('Amazon AWS Secret Access Key'),
      '#description' => t('You must sign up for an Amazon AWS account to use the Product Advertising Service.'),
      '#default_value' => $config->get('amazon_aws_secret_access_key'),
      '#required' => TRUE,
    ];

    // Associate ID || Textfield.
    $form['amazon_associate_id'] = [
      '#type' => 'textfield',
      '#title' => t('Associate ID'),
      '#description' => t('The Associate ID is used to track referral bonuses when shoppers purchase Amazon products via your site.'),
      '#default_value' => $config->get('amazon_associate_id'),
      '#required' => TRUE,
    ];

    $form['add']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save Configuration'),
      '#button_type' => 'primary',
    ];

    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('amazon.settings')
      ->set('amazon_aws_access_key', $form_state->getValue('amazon_aws_access_key'))
      ->set('amazon_aws_secret_access_key', $form_state->getValue('amazon_aws_secret_access_key'))
      ->set('amazon_associate_id', $form_state->getValue('amazon_associate_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
